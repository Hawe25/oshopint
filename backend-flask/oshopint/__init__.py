from flask import Flask
from oshopint import credentials
from oshopint.model.database import Session
from oshopint.model.tables.sku import *

app = Flask(__name__)

session = Session()

result = session.query(sku_prefix).all()

for row in result:
	print("Name: ",row.name, "Description:",row.description, "id:",row.prefix_id)
	

# engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])

# session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Base = declarative_base()

from oshopint import views

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login'))

    return wrap