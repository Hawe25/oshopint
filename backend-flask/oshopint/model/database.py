from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
# import sys
import pymysql
# sys.path.append("/home/hawe/Project/oshopint/backend-flask")
from oshopint import credentials
from oshopint.model.base import Base

SQLALCHEMY_DATABASE_URI = ("mysql+pymysql://"+ credentials.sql_user + ":" 
                                         + credentials.sql_pass + "@" 
                                         + credentials.sql_server + "/"
                                         + credentials.sql_database)

from oshopint.model.tables.sku import *


engine = create_engine(SQLALCHEMY_DATABASE_URI)

Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)


print("db")
# sku1 = sku_prefix(name="FFF",description="baju FFF")
# session.add(sku1)
# session.rollback()

# result = session.query(sku_prefix).all()

# for row in result:
# 	print("Name: ",row.name, "Description:",row.description, "id:",row.prefix_id)