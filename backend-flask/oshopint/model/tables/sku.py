from datetime import datetime
from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, Boolean
from sqlalchemy.orm import relationship, backref
from oshopint.model.base import Base

class sku_prefix(Base):
	__tablename__ = "sku_prefix"
	prefix_id  = Column(Integer, primary_key=True)
	name  = Column(String(3), nullable=False)
	description  = Column(String(50), nullable=False)
	is_active = Column(Boolean(), default=True)
	created_at = Column(DateTime(), default=datetime.now())
	updated_at = Column(DateTime(), default=datetime.now(), onupdate=datetime.now())

	items = relationship("sku_item")
	variance = relationship("sku_variance")


class sku_item(Base):
	__tablename__ = "sku_item"
	prefix_id = Column(Integer, ForeignKey(sku_prefix.prefix_id))
	item_id = Column(Integer, primary_key=True)
	name = Column(String(5), nullable=False)
	description = Column(String(50), nullable=False)
	is_active = Column(Boolean(), default=True)
	created_at = Column(DateTime(), default=datetime.now())
	updated_at = Column(DateTime(),default=datetime.now(), onupdate=datetime.now()) 

	variance = relationship("sku_variance")
	# prefix = relationship("sku_prefix")


class sku_variance(Base):
	__tablename__ = "sku_variance"
	prefix_id = Column(Integer, ForeignKey(sku_prefix.prefix_id))
	item_id = Column(Integer, ForeignKey(sku_item.item_id))
	sku = Column(String(15), primary_key=True)
	category = Column(Integer(), nullable=False)
	name = Column(String(4), nullable=False)
	description = Column(String(50), nullable=False)
	is_active = Column(Boolean(), default=True)
	created_at = Column(DateTime(), default=datetime.now())
	updated_at = Column(DateTime(), default=datetime.now(), onupdate=datetime.now())

	# item = relationship("sku_variance")
	# prefix = relationship("sku_prefix")