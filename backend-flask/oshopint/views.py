from oshopint import app
from oshopint import credentials
# from config import *
from flask import redirect, url_for, render_template, request, session
import os

@app.route("/")
def index():

    # Use os.getenv("key") to get environment variables
    app_name = os.getenv("APP_NAME") 
    # if app_name:
    #     return f"Hello from {app_name} running in a Docker container behind Nginx!"

    return render_template("pages/contents/index.html")


@app.route("/product")
# @login_required
def productPage():

    return render_template("pages/contents/product.html")


@app.route("/product/new")
# @login_required
def addProductCategoryPage():

    return render_template("pages/contents/product/new_product.html")


@app.route("/order")
# @login_required
def orderPage():
    print("test")
    return app.config['SQLALCHEMY_DATABASE_URI'];


@app.route("/login",methods=["POST","GET"])
def loginPage():

    return render_template('pages/login.html')


@app.route("/logout")
def logoutPage():

    return "halaman logout"


@app.errorhandler(404)
def page_not_found(e):
    return render_template('pages/errors/404.html')


@app.errorhandler(500)
def server_error(e):
    return render_template('pages/errors/500.html')